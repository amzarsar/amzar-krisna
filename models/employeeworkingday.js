'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EmployeeWorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      EmployeeWorkingDay.belongsTo(models.Employee, {
        foreignKey: "employee_id",
        as: "employee",
      });
      EmployeeWorkingDay.belongsTo(models.WorkingDay, {
        foreignKey: "workingday_id",
        as: "workingday"
      })
    }
  };
  EmployeeWorkingDay.init({
    employee_id: DataTypes.INTEGER,
    workingday_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'EmployeeWorkingDay',
  });
  return EmployeeWorkingDay;
};