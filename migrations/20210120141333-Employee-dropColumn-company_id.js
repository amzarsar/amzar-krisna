'use strict';

const { sequelize } = require("../models");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn("Employees", "company_id");
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn("Employees", "company_id", Sequelize.STRING)
  }
};
