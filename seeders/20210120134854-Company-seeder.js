'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('company', [
      {
        company_name: 'TED',
        company_address: 'Jakarta',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      ],
      {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('company', null, {});
  }
};
