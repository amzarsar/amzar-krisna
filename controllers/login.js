const dataLogin = require('../models').Login
const JWT = require("jsonwebtoken");
const bcrypt = require('bcrypt');
require("dotenv/config");


module.exports = {
    async login(req,res) {
        try {
            const email = req.body.email;

            const checkEmail = await dataLogin.findOne({
                where: {email: email}
            })
            const jwtnameemail = {
                name: checkEmail.name,
                email: checkEmail.email
            }

            const registerredPassword = checkEmail.password;
            const password = req.body.password;
            const comparredPassword = bcrypt.compareSync(password,registerredPassword);

            if(!checkEmail) {
                res.status(400).send({
                    message: "your email is not registered"
                })
            }
            else {
                if(!comparredPassword) {
                    res.status(400).send({
                        message: "password not match"
                    })
                }
                else {
                    console.log("user",checkEmail);
                    const token = JWT.sign(
                        {jwtnameemail},
                        process.env.SECRETKEY,
                        {expiresIn: '30s'}
                    );

                    console.log("token",token);

                    res.status(200).send({
                        message: "your email is right",
                        token
                    })
                }
            }
        }
        catch(error) {
            console.log("error",error)
            res.status(500).send({
                message: "email salah"
            })
        }

    }
}
