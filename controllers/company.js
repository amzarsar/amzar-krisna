const Company = require('../models').Company
const Employee = require('../models').Employee

class CompanyController {
    constructor() {}
    
    async readAll(req,res,next) {
        try {
            const company = await Company.findAll()
            res.status(200).json({
                status: true,
                data: company})
        }
        catch(error) {
            res.status(500).json (error)
        }
    }
    async readOneById(req,res,next) {
        try {
            const company_id = req.params.id;
            const company = await Company.findOne({
                where: {id: company_id}
                , include: [{
                    model: Employee,
                    as: "employee"
                }]
            })
            if (company!=null) {
                res.status(200).json({
                    status: true,
                    company
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `Company with ID ${req.params.id} not found!`,
                })
            }
        }
        catch(error) {
            res.status(500).json (error)
        }
    }
    async createCompany(req,res,next) {
        try {
            const check_companies = await Company.findOne({where: {company_name: req.body.company_name}})
            if(check_companies) {
                // console.log(`Company Name ${req.body.company_name} already exsist`)
                res.status(200).json({
                    status: false,
                    message: `Company with Name ${req.body.company_name} already exsist!`,
                })
            }
            else {
                // console.log(`Company Name ${req.body.company_name} not exsist`)
                 const company = await Company.create({
                    company_name: req.body.company_name,
                    company_address: req.body.company_address
                })
                res.status(200).json({
                    status: true,
                    company
                })
            }
           
        }
        catch(error) {
            res.status(500).json (error)
        }
    }
    async updateCompany(req,res,next) {
        try {
            const check_companies = await Company.findByPk(req.params.id)
            if(check_companies) {
                const company = await Company.update({
                    company_name: req.body.company_name || company_name,
                    company_address: req.body.company_address || company_address,
                },
                {
                    where: {id : req.params.id}
                })
                const update_companies = await Company.findByPk(req.params.id)
                res.status(200).json({
                    status: true,
                    check_companies,
                    message: `Company with id ${req.params.id} updated to`,
                    update_companies
                })
            }
            else {
                res.status(200).json({
                    status: false,
                    message: `Company with ID ${req.params.id} not found!`,
                })
            }
        }
        catch(error) {
            res.status(500).json (error)
        }
    }
    async deleteCompany(req,res,next) {
        try {
            const check_companies = await Company.findByPk(req.params.id)
            if(check_companies) {
                const company = await Company.destroy({where: {id: req.params.id}})
                res.status(200).json({
                    status: true,
                    check_companies,
                    message: `Company with ID ${req.params.id} deleted!`
                })
            }
            else {
                res.status(200).json({
                    status: false,
                    message: `Company with ID ${req.params.id} not found!`
                })
            }
        }
        catch(error) {
            res.status(500).json (error)
        }
    }
}

module.exports = CompanyController